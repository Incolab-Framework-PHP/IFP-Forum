<?php

namespace Library\Entities;

class ForumPosts extends \Library\Entity
{
    protected $auteur,
    $contenu,
    $sujet,
    $dateAjout,
    $dateModif;
    
    const AUTEUR_INVALIDE = 1;
    const CONTENU_INVALIDE = 2;
    
    public function isValid()
    {
        return !(empty($this->auteur) || empty($this->contenu));
    }
    
    // Setters
    
    public function setAuteur($auteur)
    {
        if(!is_string($auteur) || empty($auteur))
        {
            $this->erreurs[] = self::AUTEUR_INVALIDE;
        }
        else
        {
            $this->auteur = $auteur;
        }
    }
    
    public function setContenu($contenu)
    {
        if(!is_string($contenu) || empty($contenu))
        {
            $this->erreurs[] = self::CONTENU_INVALIDE;
        }
        else
        {
            $this->contenu = $contenu;
        }
    }
    
    public function setSujet($sujet)
    {
        $this->sujet = $sujet;
    }
    
    public function setDateAjout(\DateTime $dateAjout)
    {
        $this->dateAjout = $dateAjout;
    }
    
    public function setDateModif(\DateTime $dateModif)
    {
        $this->dateModif = $dateModif;
    }
    
    
    // Getters
    
    public function auteur()
    {
        return $this->auteur;
    }
    
    public function contenu()
    {
        return $this->contenu;
    }
    
    public function sujet()
    {
        return $this->sujet;
    }
    
    public function dateAjout()
    {
        return $this->dateAjout;
    }
    
    public function dateModif()
    {
        return $this->dateModif;
    }
}