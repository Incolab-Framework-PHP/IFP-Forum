<?php

namespace Library\Models;

use \Library\Entities\ForumSujets;

abstract class ForumSujetsManager extends \Library\Manager
{
    /*
     * Methode permattant d'obtenir la liste des sujets du forum
     * @param $debut int Le premier sujet a selectionner
     * @param $limite int Le nombre de sujets a afficher
     * @return array La liste des sujets, chaque entrée est une instance de ForumSujets
     */
    abstract public function getList($debut = -1, $limite = -1);
    
    /*
     * Methode permettant d'obtenir un un sujet unique
     * @param $id int l'ID du sujet
     * @return array l'instance du sujet selectionné
     */
    abstract public function getUnique($id);
    
    abstract protected function add(ForumSujets $forumSujets);
    
    abstract protected function modify(ForumSujets $forumSujets);
    
    /*
     * Methode permettant d'ajouter un article
     */
    
    public function save(ForumSujets $forumSujets)
    {
        if($forumSujets->isValid())
        {
            $forumSujets->isNew() ? $this->add($forumSujets) : $this->modify($forumSujets);
        }
        else
        {
            throw new \RuntimeException('Le sujet doit être valide pour être enregistré');
        }
    }
    
}