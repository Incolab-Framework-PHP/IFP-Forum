<?php

namespace Library\Models;

use \Library\Entities\ForumPosts;

abstract class ForumPostsManager extends \Library\Manager
{
    /* Methode permettant de recupérer une liste de reponses.
     * @param $sujet Le sujet sur lequel on veut recupérer les réponses
     * @return array
     */
    abstract public function getListOf($sujet);
    
    
    /*
     * Méthode permettant d'enregistrer un Post.
     * @param $post Le post à enregistrer
     * @return void
     */
    
    abstract protected function add(ForumPosts $post);
    
    
    public function save(ForumPosts $post)
    {
        if($post->isValid())
        {
            $post->isNew() ? $this->add($post) : $this->modify($post);
        }
        else
        {
            throw new \RuntimeException('Le commentaire doit être valide pour être enregistré.');
        }
    }
    
}
