<?php

namespace Library\Models;

use \Library\Entities\ForumSujets;

class ForumSujetsManager_PDO extends ForumSujetsManager
{
    public function getList($debut = -1, $limite = -1)
    {
        $sql = 'SELECT id, auteur, titre, contenu, dateAjout, dateModif, dateDernierPost FROM forum_sujets ORDER BY dateDernierPost DESC';
        
        if($debut != -1 || $limite != -1)
        {
            $sql .= ' LIMIT '.(int) $limite.' OFFSET ';(int) $debut;
        }
        
        $q = $this->dao->query($sql);
        $q->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Library\Entities\ForumSujets');
        
        $listeSujets = $q->fetchAll();
        
        foreach($listeSujets as $sujet)
        {
            $sujet->setDateAjout(new \DateTime($sujet->dateAjout()));
            $sujet->setDateModif(new \DateTime($sujet->dateModif()));
            $sujet->setDateDernierPost(new \DateTime($sujet->dateDernierPost()));
        }
        
        $q->closeCursor();
        
        return $listeSujets;
        
    }
    
    public function getUnique($id)
    {
        $q = $this->dao->prepare('SELECT id, auteur, titre, contenu, dateAjout, dateModif, dateDernierPost FROM forum_sujets WHERE id = :id');
        
        $q->bindValue(':id', (int) $id, \PDO::PARAM_INT);
        $q->execute();
        $q->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Library\Entities\ForumSujets');
        
        if($sujet = $q->fetch())
        {
            $sujet->setDateAjout(new \DateTime($sujet->dateAjout()));
            $sujet->setDateModif(new \DateTime($sujet->dateModif()));
            $sujet->setDateDernierPost(new \DateTime($sujet->dateDernierPost()));
        }
        
        $q->closeCursor();
        
        return $sujet;
    }
    
    protected function add(ForumSujets $forumSujets)
    {
        $q = $this->dao->prepare('INSERT INTO forum_sujets SET auteur = :auteur, titre = :titre, contenu = :contenu, dateAjout = CURRENT_TIMESTAMP(), dateModif = CURRENT_TIMESTAMP(), dateDernierPost = CURRENT_TIMESTAMP()');
        
        $q->bindValue(':auteur', $forumSujets->auteur());
        $q->bindValue(':titre', $forumSujets->titre());
        $q->bindValue(':contenu', $forumSujets->contenu());
        
        $q->execute();
        
        $forumSujets->setId($this->dao->lastInsertId());
    }
    
    protected function modify(ForumSujets $forumSujets)
    {
        
    }
}