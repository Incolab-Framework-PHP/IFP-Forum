<?php

namespace Library\Models;

use \Library\Entities\ForumPosts;

class ForumPostsManager_PDO extends ForumPostsManager
{
    public function getListOf($sujet)
    {
        if(!ctype_digit($sujet))
        {
            throw new \InvalidArgumentException('L\'identifiant du sujet passé doit être un nombre entier valide');
        }
        
        $q = $this->dao->prepare('SELECT id, auteur, contenu, dateAjout FROM forum_posts WHERE sujet = :sujet');
        $q->bindValue(':sujet', $sujet, \PDO::PARAM_INT);
        $q->execute();
        
        $q->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Library\Entities\ForumPosts');
        
        $posts = $q->fetchAll();
        
        foreach($posts as $post)
        {
            $post->setDateAjout(new \DateTime($post->dateAjout()));
            $post->setDateModif(new \DateTime($post->dateModif()));
        }
        
        $q->closeCursor();
        
        return $posts;
    }
    
    protected function add(ForumPosts $post)
    {
        $q = $this->dao->prepare('INSERT INTO forum_posts SET sujet = :sujet, auteur = :auteur, contenu = :contenu, dateAjout = CURRENT_TIMESTAMP(), dateModif = CURRENT_TIMESTAMP()');
        
        $q->bindValue(':sujet', $post->sujet());
        $q->bindValue(':auteur', $post->auteur());
        $q->bindValue(':contenu', $post->contenu());
        
        $q->execute();
        
        $post->setId($this->dao->lastInsertId());
        
        $q->closeCursor();
        
        $qlastpost = $this->dao->prepare('UPDATE forum_sujets SET dateDernierPost = NOW() WHERE id = :id');
        
        $qlastpost->bindValue(':id', $post->sujet(), \PDO::PARAM_INT);
        $qlastpost->execute();
        $qlastpost->closeCursor();
    }
}