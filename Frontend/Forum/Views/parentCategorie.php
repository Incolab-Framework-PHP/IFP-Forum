<section class="row">
    <div class="large-12 columns">
        <h1 class="text-center"><?php echo $dataParentCategorie['titreCategorie']; ?></h1>
    </div>
    <div class="large-12 columns">
        <div class="row">
            <div class="large-3 medium-5 columns left">
                <h3><a href="/forum">Accueil</a></h3>
            </div>
        </div>
    </div>
    <!--
    <div class="large-3 medium-4 columns right">
        <h3 class="text-center"><a href="forum/insert-sujet.html">[Inserer un Sujet]</a></h3>
    </div>
    -->
    <div class="large-12 columns panelGrey">
        <?php
        if(!empty($categories))
        {
            foreach($categories as $key => $categorie)
            {
                if($key != 0){ echo '<hr/>'; }
            ?>
                <div class="">
                    <div class="row">
                        <div class="large-10 medium-10 columns">
                            <h4 class="#"><a href="/forum/<?php echo $dataParentCategorie['categorie'];?>/<?php echo $categorie['categorie']; ?>.html"><?php echo $categorie['titreCategorie']; ?></a></h4>
                            <p><?php echo $categorie['texteCategorie']; ?></p>
                        </div>
                        <div class="large-2 medium-2 columns text-center">
                            <p>Sujets:<br/>
                            <?php echo $countCategorie[$categorie['categorie']]; ?> </p>
                        </div>
                    </div>
                </div>
                
                <!--
                    <h4 class="#"><a href="/forum/<?php echo $dataParentCategorie['categorie'];?>/<?php echo $categorie['categorie']; ?>.html"><?php echo $categorie['titreCategorie']; ?></a></h4>
                    <p><?php echo $categorie['texteCategorie']; ?></p>
                -->
            <?php
            }
        }
        else
        { ?>
            <p>Aucune catégorie n'a été défini pour le moment.</p>
        <?php
        } ?>
        </div>
    </div>
</section>