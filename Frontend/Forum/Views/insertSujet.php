<section class="row">
    <div class="large-12 columns">
        <h2 class="text-center">Ajouter un Sujet</h2>
        <?php
        if($user->isAuthenticated())
        { ?>
        <form action="" method="post">
            <p class="panel">
                <b><?php echo $user->getPseudo(); ?></b>, vous allez poster un Sujet de discussion dans [<?php echo $categorie['titreParentCategorie'].']/['.$categorie['titreCategorie']; ?>]
            </p>
            <p>  
                <?php if(isset($erreurs) && in_array(\Library\Entities\ForumSujet::TITRE_INVALIDE, $erreurs)){ echo "Le titre est invalide.<br/>"; } ?>
                <label>Titre</label>
                <input type="text" name="titre" value="<?php if(isset($sujet)){ echo htmlspecialchars($sujet['titre']);} ?>" /><br/>
                
                <?php if(isset($erreurs) && in_array(\Library\Entities\ForumSujet::CONTENU_INVALIDE, $erreurs)){ echo "Le contenu est invalide.<br/>"; } ?>
                <label>Contenu</label>
                <textarea name="contenu" rows="7" cols="50"><?php if(isset($sujet)){ echo htmlspecialchars($sujet['contenu']); }?></textarea>
                
                <input type="submit" value="Ajouter un Sujet" />
                
            </p>
        </form>
        <?php
        }else{ ?>
        <p class="text-center">Vous devez être connecté pour poster un sujet.<br/>
        Pour vous connecter, <a href="/mon-compte">cliquez-ici</a>.</p>
        <?php } ?>
    </div>
</section>