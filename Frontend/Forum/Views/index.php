<section class="row">
    <div class="large-12 columns">
        <h1 class="text-center">Accueil du Forum</h1>
    </div>
    <div class="large-12 columns">
        <h4 class="text-center cadre_parentC_forum"><span id="sujets">Les derniers sujets</span> | <span id="posts" class="active">Les derniers posts</span></h4>
        <div id="owl-example" class="owl-carousel">
                <?php
                foreach($lastPosts as $element)
                { ?>
                <div class="large-12 columns">
                    <h5 class="text-center"><a href="/forum/<?php echo  $sujetsOfLP[$element['sujet']]['parentCategorie'].'/'.$sujetsOfLP[$element['sujet']]['categorie'].'/sujet-'.$element['sujet'].'.html'; ?>"><?php echo $sujetsOfLP[$element['sujet']]['titre']; ?></a></h5>
                    <p><?php echo $element['contenu']; ?><br/><br/><em><a href="/forum/<?php echo  $sujetsOfLP[$element['sujet']]['parentCategorie'].'/'.$sujetsOfLP[$element['sujet']]['categorie'].'/sujet-'.$element['sujet'].'.html'; ?>">Lire la suite...</a></em></p>
                    <em>Auteur: <?php echo $element['auteur'].' | '.$element['dateAjout']; ?></em>
                </div>
            <?php } ?>
        </div>
    </div>
        <?php
        if(!empty($parentCategorie))
        {
            foreach($parentCategorie as $parent)
            { ?>
                <div class="large-12 columns">
                    
                    <h4 class="text-center cadre_parentC_forum"><a href="/forum/<?php echo $parent['categorie'];?>.html"><?php echo $parent['titreCategorie']; ?></a></h4>
                    <div class="cat_forum">
                        <?php
                        if(!empty($categories))
                        {
                            $i = 0;
                            foreach($categories as $categorie)
                            {
                                if($categorie['parentCategorie'] == $parent['categorie'])
                                {
                                    if($i != 0){ echo '<hr class="hr_forum"/>';}
                                    ?>
                                    <div class="row">
                                        <div class="large-10 medium-10 columns">
                                            <h5><a href="forum/<?php echo $parent['categorie']; ?>/<?php echo $categorie['categorie']; ?>.html"><?php echo htmlspecialchars($categorie['titreCategorie']); ?></a></h5>
                                            <p><?php echo $categorie['texteCategorie']; ?></p>
                                        </div>
                                        <div class="large-2 medium-2 columns text-center">
                                            <p>Sujets:<br/>
                                            <?php echo $countCategorie[$categorie['categorie']]; ?> </p>
                                        </div>
                                    </div>
                                    
                                <?php
                                    $i++;
                                }
                            }
                        }
                        ?>
                    </div>
                </div>
            <?php
            }
        }
        else
        { ?>
        <div class="large-12 columns">
            <p>Aucune catégorie n'a été défini pour le moment.</p>
        </div>
        <?php
        } ?>
</section>
<?php $jsScripts = file(realpath('.').'/JsScripts/index.html'); ?>