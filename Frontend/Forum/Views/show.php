<section class="row">
    <div class="large-12 columns">
        <h1 class="text-center"><?php echo htmlspecialchars($sujet['titre']); ?></h1>
    </div>
    <div class="large-12 columns">
        <div class="row">
            <div class="large-12 columns left">
                <h3><a href="/forum">Accueil</a> / <a href="/forum/<?php echo $categorie['parentCategorie'].'.html'; ?>"><?php echo $categorie['titreParentCategorie']; ?></a> / <a href="/forum/<?php echo $categorie['parentCategorie'].'/'.$categorie['categorie']; ?>.html"><?php echo $categorie['titreCategorie']; ?></a></h3>
            </div>
        </div>
        <div class="large-12 columns">
            
        </div>
        
    </div>
    <table class="large-12 columns">
        <thead>
            <tr>
                <th width="120px" class="text-center">Auteur</th>
                <th class="text-center">Message</th>
                <th width="120px" class="text-center hide-for-small">Date Ajout</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center"><p><?php echo nl2br(htmlspecialchars($sujet['auteur'])); ?></p></td>
                <td><p><?php echo nl2br(htmlspecialchars($sujet['contenu'])); ?></p></td>
                <td class="text-center hide-for-small"><?php echo $sujet['dateAjout']->format('d/m/y (H\hi)'); ?></td>
            </tr>
        </tbody>
    </table>
        <div class="large-12 columns right">
            <h2 class="text-center panelGrey">Réponses</h2>
        </div>
        
        
            <?php
            if(!empty($posts))
            { ?>
            
            <table class="large-12 columns">
                <thead>
                    <tr>
                        <th width="120px" class="text-center">Auteur</th>
                        <th class="text-center">Message</th>
                        <th width="120px" class="text-center hide-for-small">Date Ajout</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach($posts as $post)
                    { ?>
                    <tr>
                        <td class="text-center"><p><?php echo htmlspecialchars($post['auteur']); ?></p></td>
                        <td><p><?php echo nl2br(htmlspecialchars($post['contenu'])); ?></p></td>
                        <td class=" text-center hide-for-small"><?php echo $post['dateAjout']->format('d/m/y (H\hi)'); ?></td>
                    </tr>
                    <?php
                    }
                    ?>
                </tbody>
            </table>
            <?php
            }
            else
            { ?>
            
            <div class="large-12 columns">
                <p>Aucune réponse n'a été postée pour le moment. Soyez le premier à en ajouter une.</p>
            </div>
            <?php
            }
            ?>
            <div class="large-6 columns left cadreNoirPad">
                <h3 class="text-center">Répondre</h3>
                <?php require '_form_insertPost.php'; ?>
            </div>
        </div>
    </div>
</section>