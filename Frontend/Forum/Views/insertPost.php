<section class="row">
    <div class="large-12 columns">
        <h1 class="text-center">Ajouter un post</h1>
    </div>
    <div class="large-12 columns">
        <div class="row">
            <div class="large-3 medium-3 columns right">
                <h3 class="text-center"><a href="sujet-<?php echo $post['sujet']; ?>.html">[Retour au Sujet]</a></h3>
            </div>
        </div>
    </div>
    <div class="large-12 columns">
         <?php
        if($user->isAuthenticated())
        { ?>
        <form action="" method="post">
            <p class="text-center">Vous écrivez en tant que <?php echo $user->getPseudo(); ?></p>
            <p>
                <?php if(isset($erreurs) && in_array(\Library\Entities\ForumPosts::CONTENU_INVALIDE, $erreurs)){ echo "Le contenu est invalide.<br/>"; } ?>
                <label>Contenu</label>
                <textarea name="contenu" rows="7" cols="50"><?php if(isset($post)){ echo htmlspecialchars($post['contenu']); }?></textarea>
                
                <input type="submit" value="Repondre" />
                
            </p>
        </form>
        <?php
        }else{ ?>
        <p class="text-center">Vous devez être connecté pour commenter le Sujet<br/>
        Pour vous connecter, <a href="/mon-compte">cliquez-ici</a>.</p>
        <?php } ?>
    </div>
</div>
</section>