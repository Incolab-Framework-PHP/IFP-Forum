 <?php
    if($user->isAuthenticated())
    { ?>

<form action="/forum/insert-post-<?php echo $sujet['id']; ?>.html" method="post">
    <p>
    Vous écrivez en tant que <?php echo $user->getPseudo(); ?><br/>
    <?php if(isset($erreurs) && in_array(\Library\Entities\ForumPosts::CONTENU_INVALIDE, $erreurs)){ echo "Le contenu est invalide.<br/>"; } ?>
        <label>Contenu</label>
        <textarea name="contenu" rows="7" cols="50"><?php if(isset($proviPost)){ echo htmlspecialchars($proviPost['contenu']); }?></textarea>
        
        <input class="button radius" type="submit" value="Repondre" />
    </p>
</form>

<?php
    }else{ ?>
<p class="text-center">Vous devez être connecté pour commenter le Sujet<br/>
Pour vous connecter, <a href="/mon-compte">cliquez-ici</a>.</p>
    <?php } ?>