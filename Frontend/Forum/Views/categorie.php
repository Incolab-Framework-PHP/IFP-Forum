<section class="row">
    <div class="large-12 columns">
        <h1 class="text-center"><?php echo $categorie['titreCategorie']; ?></h1>
    </div>
    <div class="large-6 medium-4 columns left">
        <h3><a href="/forum">Accueil</a> / <a href="/forum/<?php echo $categorie['parentCategorie'].'.html'; ?>"><?php echo $categorie['titreParentCategorie']; ?></a></h3>
    </div>
    <div class="large-3 medium-4 columns right">
        <h3 class="text-center"><a href="/forum/<?php echo $categorie['parentCategorie'].'/'.$categorie['categorie'].'/insert-sujet.html'; ?>">[Inserer un Sujet]</a></h3>
    </div>
        
        <?php
        if(!empty($listeSujets))
        {
            foreach($listeSujets as $sujet)
            { ?>
            <div class="large-12 columns">
                <div class="panelGrey">
                <div class="row" data-equalizer>
                    <div class="large-6 columns" data-equalizer-watch>
                        <h4 class="#"><a href="/forum/<?php echo $sujet['parentCategorie'].'/'.$sujet['categorie'].'/sujet-'.$sujet['id']; ?>.html"><?php echo $sujet['titre']; ?></a></h4>
                        <p>Auteur: <?php echo $sujet['auteur']; ?></p>
                    </div>
                    <div class="large-3 columns text-center" data-equalizer-watch>
                        <p>Dernier post:<br/>
                        <?php echo $sujet["dateDernierPost"]->format('d/m/Y à H\hi'); ?></p>
                    </div>
                </div>
                
                </div>
            </div>
            <?php
            }
        }
        else
        { ?>
        <div class="large-12 columns">
            <p class="text-center">Aucune Sujet n'a été posé pour le moment.<br/>
            Soyez le premier à en créer un!</p>
        </div>
        <?php
        } ?>
        
    </div>
</section>