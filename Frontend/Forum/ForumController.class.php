<?php

namespace Applications\Frontend\Modules\Forum;

class ForumController extends \Library\BackController
{
    public function executeOldSujets(\Library\HTTPRequest $request)
    {
        // On récupère le manager des Sujets du Forum
        $manager = $this->managers->getManagerOf('ForumSujets');
        // On recupere le sujet
        $sujet = $manager->getSujetOfId($request->getData('id'));
        
        if(empty($sujet))
        {
            $this->app->httpReponse()->redirect404();
        }
        else
        {
            $this->app->httpReponse()->redirect301("http://minetest-france.fr/forum/".$sujet["parentCategorie"]."/".$sujet["categorie"]."/sujet-".$sujet["id"].".html");
            // On récupere le manager des posts et en recupere les post pour le sujet donné
            $posts = $this->managers->getManagerOf('ForumPosts')->getListOf($sujet->id());
            $this->page->addVar('title', $sujet->titre());
            $this->page->addVar('sujet', $sujet);
            $this->page->addVar('posts', $posts);
        }
    }
    
    public function executeIndex(\Library\HTTPRequest $request)
    {
        // On ajoute le titre a la page
        $this->page->addVar('title', 'Forum');
        
        // On recupere le manager de ForumCategorie
        $managerFC = $this->managers->getManagerOf('ForumCategorie');
        
        $listCategorie = $managerFC->getListOfCategories();
        
        //on recup le manager de ForumSujets et compte le nombres de Sujets dans chaques catégories
        $managerFS = $this->managers->getManagerOf('ForumSujets');
        foreach($listCategorie as $categorie)
        {
            $countCategorie[$categorie['categorie']] = $managerFS->countSujetsOf($categorie['categorie']);
        }
        
        $this->page->addVar("parentCategorie", $managerFC->getListOfParents());
        $this->page->addVar("categories", $listCategorie);
        $this->page->addVar("countCategorie", $countCategorie);
        
        // Les derniers posts pour le slideshow texte
        $lastPosts = $this->managers->getManagerOf('ForumPosts')->getList(0,5);
        $SujetsOfLP;
        foreach($lastPosts as $post)
        {
            $sujetsOfLP[$post['sujet']] = $managerFS->getSujetOfId($post['sujet']);
           
            $nbCaract = 250;
            if(strlen($post->contenu()) > $nbCaract)
            {
                //$debut = substr($post->contenu(), 0, $nbCaract);
                //$debut = substr($debut, 0, strrpos($debut, ' ')).'...';
                
                $debut = substr(substr($post->contenu(),0,$nbCaract),0,strrpos(substr($post->contenu(),0,$nbCaract)," "))." (...)";
                $post->setContenu($debut);
            }
        }
        
        $this->page->addVar('lastPosts', $lastPosts);
        $this->page->addVar('sujetsOfLP' , $sujetsOfLP);
        
    }
    
    public function executeParentCategorie(\Library\HTTPRequest $request)
    {
        // On recupere le manager de ForumCategorie
        $managerFC = $this->managers->getManagerOf('ForumCategorie');
        
        // On recupere la categorie parente demandé, si elle n'existe pas -> 404
        $categorieP = $managerFC->getParent($request->getData('parentCategorie'));

        
        if(empty($categorieP))
        {
            $this->app->httpReponse()->redirect404();
        }
        
        $listCategorie = $managerFC->getChildsOf($request->getData('parentCategorie'));
        
        //on recup le manager de ForumSujets et compte le nombres de Sujets dans chaques catégories
        $managerFS = $this->managers->getManagerOf('ForumSujets');
        foreach($listCategorie as $categorie)
        {
            $countSujets[$categorie['categorie']] = $managerFS->countSujetsOf($categorie['categorie']);
        }
        
        
        // Si la catégorie parente existe, on passe le titre les les variables
        $this->page->addVar('title', 'Forum | '.$categorieP['titreCategorie']);
        $this->page->addVar('dataParentCategorie', $categorieP);
        $this->page->addVar('categories', $listCategorie);
        $this->page->addVar('countCategorie', $countSujets);
        
    }
    
    public function executeCategorie(\Library\HTTPRequest $request)
    {
        $managerFC = $this->managers->getManagerOf('ForumCategorie');
        
        $categorie = $managerFC->getChild($request->getData('parentCategorie'), $request->getData('categorie'));
        if(empty($categorie))
        {
            $this->app->httpReponse()->redirect404();
        }
        
        // Si la categorie existe, on passe les variables
        $this->page->addVar('title', 'Forum '.$categorie['titreParentCategorie'].' | '.$categorie['titreCategorie']);
        $this->page->addVar('categorie', $categorie);
        $this->page->addVar('listeSujets', $this->managers->getManagerOf('ForumSujets')->getListOf($categorie['parentCategorie'], $categorie['categorie']));
        
    }
    
    public function executeShow(\Library\HTTPRequest $request)
    {
        // On récupère le manager des Sujets du Forum
        $manager = $this->managers->getManagerOf('ForumSujets');
        // On recupere le sujet
        $sujet = $manager->getUnique($request->getData('categorie'), $request->getData('parentCategorie'), $request->getData('id'));
        
        if(empty($sujet))
        {
            $this->app->httpReponse()->redirect404();
        }
        else
        {
            // On récupere le manager des posts et en recupere les post pour le sujet donné
            $posts = $this->managers->getManagerOf('ForumPosts')->getListOf($sujet->id());
            $this->page->addVar('title', $sujet->titre());
            $this->page->addVar('sujet', $sujet);
            $this->page->addVar('posts', $posts);
            $this->page->addVar('categorie', $this->managers->getManagerOf('ForumCategorie')->getChild($request->getData('parentCategorie'), $request->getData('categorie')));
        }
        
    }
    
    public function executeInsertSujet(\Library\HTTPRequest $request)
    {
        // On verifie que la catégorie existe, sinon -> 404
        $categorie = $this->managers->getManagerOf('ForumCategorie')->getChild($request->getData('parentCategorie'), $request->getData('categorie'));
        if(empty($categorie))
        {
            $this->app->httpReponse()->redirect404();
        }
        
        // Verifie que l'user à les privs necessaires pour poster dans cette sous cat
        $peuxWrite = false;
        foreach($categorie['writePrivs'] as $writePrivs)
        {
            if(in_array($writePrivs, $this->userClass()->getPrivs()))
            {
                //actions acceptés
                $peuxWrite = true;
            }
        }
        
        if($peuxWrite)
        {
            if($request->postExists('contenu') && $this->app->user()->isAuthenticated())
            {
                // on ajoute le titre a la page
                $this->page->addVar('title', 'Inserer un sujet');
                
                $sujet = new \Library\Entities\ForumSujets(array(
                    'auteur' => $this->app->user()->getPseudo(),
                    'titre' => $request->postData('titre'),
                    'contenu' => $request->postData('contenu'),
                    'parentCategorie' => $request->getData('parentCategorie'),
                    'categorie' => $request->getData('categorie')
                ));
                
                if($sujet->isValid())
                {
                    // Si le sujet est valide on recup le manager et on sauvegarde le sujet
                    $this->managers->getManagerOf('ForumSujets')->save($sujet);
                    
                    $this->app->user()->setFlash('Le Sujet à bien été ajouté');
                    
                    $this->app->httpReponse()->redirect('sujet-'.$sujet->id().'.html');
                }
                else
                {
                    $this->page->addVar('erreurs', $sujet->erreurs());
                }
                
                $this->page->addVar('sujet', $sujet);
                $this->page->addVar('categorie', $categorie);
            }else{
                $this->page->addVar('categorie', $categorie);
            }
        }else{
            $this->app->user()->setFlash('Vous n\'avez pas les privillèges nécessaire pour poster dans cette catégorie<br/>Verifiez que vous êtes connecté à l\'aide de l\'icone en bas à droite');
            $this->app->httpReponse()->redirect('../'.$categorie['categorie'].'.html');
        }     
    }
    
    public function executeInsertPost(\Library\HTTPRequest $request)
    {
        if($request->postExists('contenu') && $this->userClass()->isAuthenticated())
        {
            // On verifie que le sujet existe
            $sujet = $this->managers->getManagerOf('ForumSujets')->getSujetOfId($request->getData('sujet'));
            if(!empty($sujet))
            {
                $post = new \Library\Entities\ForumPosts(array(
                    'sujet' => $request->getData('sujet'),
                    'auteur' => $this->app->user()->getPseudo(),
                    'contenu' => $request->postData('contenu')
                ));
                
                if($post->isValid())
                {
                    // Si le post est valide on recup le controller et on save le post
                    $this->managers->getManagerOf('ForumPosts')->save($post);
                    
                    $this->app->user()->setFlash('Votre Post à bien été ajouté');
                    
                    
                    $this->app->httpReponse()->redirect('/forum/'.$sujet['parentCategorie'].'/'.$sujet['categorie'].'/sujet-'.$sujet['id'].'.html');
                }
                else
                {
                    $this->page->addVar('erreurs', $post->erreurs());
                }
                
                $this->page->addVar('post', $post);
            }else{
                $this->app->user()->setFlash('Le Sujet que vous voulez commenter est invalide');
                $this->app->httpReponse()->redirect('.');
            }
        }else{
            $this->app->user()->setFlash('Vous devez être authentifié pour poster sur le forum');
            $this->app->httpReponse()->redirect('.');
        }
    }
    
    // Fonctions Annexes
    public function userClass()
    {
        return $this->app->user();
    }
    
}